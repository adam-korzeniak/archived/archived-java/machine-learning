package com.adamkorzeniak.ml;

import lombok.Getter;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class Environment {

    private static final int POPULATION_LIMIT = 1000;

    private final Supplier<Agent> initialGenerationSupplier;
    private final Function<Set<Agent>, Agent> nextGenerationSupplier;

    public Environment(
                Supplier<Agent> initialGenerationSupplier,
                Function<Set<Agent>, Agent> nextGenerationSupplier) {
        this.initialGenerationSupplier = initialGenerationSupplier;
        this.nextGenerationSupplier = nextGenerationSupplier;
    }

    @Getter
    private Set<Agent> agents = new HashSet<>();

    public final void runGeneration() {
        agents.forEach(Agent::act);
        agents.forEach(Agent::calculateFitness);
    }

    public void displayGeneration() {
        Optional<Double> maxFitness = getBestFitness();
        Optional<Double> overallFitness = getOverallFitness();
        if (maxFitness.isPresent() && overallFitness.isPresent()) {
            String result = String.format("Max fitness: %f.2, Overall fitness: %f.2",
                    maxFitness.get(),
                    overallFitness.get());
            System.out.println(result);
        }
    }

    public void generateNewGeneration() {
        if (agents.isEmpty()) {
            agents = IntStream.range(0, getPopulationLimit())
                    .mapToObj(i -> initialGenerationSupplier.get())
                    .collect(Collectors.toSet());
        } else {
            Set<Agent> parents = agents.stream().sorted(Comparator.reverseOrder())
                    .limit(2).collect(Collectors.toSet());
            Agent children = nextGenerationSupplier.apply(parents);
            children.mutate();
        }
        agents.forEach(agent -> agent.setEnvironment(this));
    }

    public int getPopulationLimit() {
        return POPULATION_LIMIT;
    }

    public void displayFinalResults() {
        displayGeneration();
    }

    public Optional<Double> getOverallFitness() {
        OptionalDouble average = agents.stream()
                .mapToDouble(Agent::getFitness)
                .average();
        return average.isPresent()
                ? Optional.of(average.getAsDouble())
                : Optional.empty();
    }

    public Optional<Double> getBestFitness() {
        return agents.stream()
                .map(Agent::getFitness)
                .max(Comparator.naturalOrder());
    }
}

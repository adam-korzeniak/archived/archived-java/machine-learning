package com.adamkorzeniak.ml.travelsman2;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class TravelsmanEnvironment {

    private static final int WIDTH = 1000;
    private static final int HEIGHT = 1000;

    private final Set<Point> places;
    private final Set<TravelsmanAgent> agents;

    public TravelsmanEnvironment(int amountOfPlaces) {
        this.places = generatePlaces(amountOfPlaces);
        this.agents = generateAgents();
    }

    private Set<TravelsmanAgent> generateAgents() {
        return Set.of();
    }

    private Set<Point> generatePlaces(int amountOfPlaces) {
        Set<Point> points = new HashSet<>();
        for (int i = 0; i < amountOfPlaces; i++) {
            Point point = createPoint();
            while (points.contains(point)) {
                point = createPoint();
            }
            points.add(point);
        }
        return points;
    }

    private Point createPoint() {
        int x = (int) (Math.random() * WIDTH);
        int y = (int) (Math.random() * HEIGHT);
        return new Point(x, y);
    }
}

package com.adamkorzeniak.ml.travelsman;

import com.adamkorzeniak.ml.Environment;
import com.adamkorzeniak.ml.Simulation;

public class App {

    private static final int CITIES_AMOUNT = 50;

    public static void main(String[] args) {
        Environment environment = new TravelsmanEnvironment(CITIES_AMOUNT);
        Simulation simulation = new TravelsmanSimulation(environment);
        simulation.start();
    }
}

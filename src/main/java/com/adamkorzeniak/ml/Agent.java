package com.adamkorzeniak.ml;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public abstract class Agent implements Comparable<Agent>{

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private Double fitness;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private Environment environment;

    public abstract void act();

    public abstract void calculateFitness();

    public abstract void mutate();

    public int compareTo(Agent other) {
        return Double.compare(this.fitness, other.fitness);
    }
}
